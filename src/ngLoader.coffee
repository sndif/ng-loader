###*
# fullscreen loader for socket.io client and dhGate protocol
# 
# - injection: inject 'ng-loader' to your main angular application file
#     code : angular.module 'app', [ ..., 'ng-loader' ]
#
# - usage : add the following code to your main index html file
#     code : <loader></loader>
###
angular
  .module 'ng-loader', []
  .directive 'ngLoader', ->
    {
      restrict : 'E'
      replace: true
      template: '<div class="loader-wrap" ng-show="loading">
        <div class="loader-container ball-pulse-double">
          <div class="loader">
            <div class="ball-1"></div>
            <div class="ball-2"></div>
          </div>
        </div>
      </div>'
    }
  .factory 'requestLoading', [
    '$rootScope'
    ( $rootScope ) ->
      {
        request : ( config ) ->
          $rootScope.loading = true
          config
        requestError : ( res ) ->
          $rootScope.loading = false
          res
        response : ( res ) ->
          $rootScope.loading = false
          res
        responseError : ( res ) ->
          $rootScope.loading = false
          res

      }
  ]
  .config [
    '$httpProvider'
    ( $httpProvider ) ->
      $httpProvider.interceptors.push 'requestLoading'
      return
  ]

